# ChaosMusic
Just a list of mixes played on CCC Events.

Play all tracks random directly with mpv:

```
mpv --shuffle $(curl -s "https://git.kasiandras-dreams.de/Kasiandra/ChaosMusic/raw/branch/master/playlist" | tr '\n' ' ') --no-video
```
